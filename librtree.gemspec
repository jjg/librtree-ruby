require 'rake'

Gem::Specification.new do |s|
  s.name = 'librtree'
  s.description = <<~EOF
    A Ruby extension implementing the R-tree spatial-index of
    Guttman-Green.
  EOF
  s.version = '1.0.6'
  s.summary = 'R-tree spatial index'
  s.authors = ['J.J. Green']
  s.licenses = ['MIT']
  s.email = 'j.j.green@gmx.co.uk'
  s.homepage = 'https://gitlab.com/jjg/librtree-ruby'
  s.platform = Gem::Platform::RUBY
  s.files =
    FileList[
      'README.md',
      'CHANGELOG.md',
      'COPYING',
      'lib/rtree.rb',
      'ext/rtree/rtree.c',
      'ext/rtree/lib/**/*'
    ].to_a
  s.require_paths = [ 'lib', 'ext' ]
  s.extensions = ['ext/rtree/extconf.rb']
  s.required_ruby_version = '>= 2.0'
  s.add_development_dependency 'bundler', ['~> 2.1']
  s.add_development_dependency 'rake', ['~> 13.0']
  s.add_development_dependency 'rspec', ['~> 3.10']
  s.add_development_dependency 'simplecov', ['~> 0.22']
  s.add_development_dependency 'yard', ['~> 0.9']
  s.add_development_dependency 'rubygems-tasks', ['~> 0.2']
  s.add_development_dependency 'rake-compiler', ['~> 1.1']
  s.add_development_dependency 'steep', ['~> 1.4']
  s.add_development_dependency 'rbs', ['~> 3.2']
  s.requirements << 'The Jansson library'
end
