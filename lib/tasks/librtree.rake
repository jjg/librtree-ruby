namespace :librtree do
  desc "Update the update-script itself"
  task :refresh do
    sh('bin/librtree-update', '--verbose', '--self')
  end

  desc "Update librtree"
  task :update do
    sh('bin/librtree-update', '--verbose', '--target', 'ext/rtree/lib')
    sh('mv', 'ext/rtree/lib/rtree.c', 'ext/rtree/lib/rtree-base.c')
  end

  desc "Clean the librtree-update cache"
  task :clean do
    sh('bin/librtree-update', '--verbose', '--clean')
  end
end
