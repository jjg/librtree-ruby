describe 'RTree' do
  describe '#to_csv' do
    let(:rtree) { RTree.new(2) }
    subject { rtree.to_csv }

    it 'responds' do
      expect(rtree).to respond_to :to_csv
    end

    shared_examples 'RTree #to_csv' do |nodes|
      before { nodes.each { |node| rtree.add_rect(*node) } }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is a string' do
        expect(subject).to be_a String
      end

      it 'is binary encoded' do
        expect(subject.encoding).to be Encoding::BINARY
      end
    end

    context 'empty tree' do
      it_behaves_like('RTree #to_csv', [])
    end

    context 'non-empty tree' do
      it_behaves_like(
        'RTree #to_csv',
        [
          [1, [0, 0, 1, 1]],
          [2, [1, 1, 2, 3]]
        ]
      )
    end
  end
end
