describe 'RTree' do
  describe '#size' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :size
    end

    subject { rtree.size }

    it 'does not raise' do
      expect { subject }.to_not raise_error
    end

    it 'is an integer' do
      expect(subject).to be_an Integer
    end

    it 'is positive' do
      expect(subject).to be > 0
    end
  end
end
