require 'tempfile'

describe 'RTree' do
  describe '#postscript' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :postscript
    end

    context 'valid arguments' do

      before do
        rtree.add_rect(3, [0, 0, 2, 2])
        rtree.add_rect(4, [1, 1, 3, 3])
      end

      let(:style) do
        RTree::Style.from_a(
          [
            {
              fill: {
                rgb: [0.5, 0.5, 0.5]
              },
              stroke: {
                rgb: [0, 0, 1],
                width: 3
              }
            }
          ]
        )
      end

      shared_examples 'it writes PostScript' do

        context 'empty options' do
          let(:opts) { {} }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          describe 'the generated file' do
            before { subject }

            it 'is valid postscript' do
              expect(path).to be_valid_postscript
            end
          end
        end

        context 'explicit height' do
          let(:opts) { { height: 300 } }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          describe 'the generated file' do
            before { subject }

            it 'is valid postscript' do
              expect(path).to be_valid_postscript
            end
          end
        end

        context 'explicit width' do
          let(:opts) { { width: 300 } }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          describe 'the generated file' do
            before { subject }

            it 'is valid postscript' do
              expect(path).to be_valid_postscript
            end
          end
        end

        context 'explicit width and height' do
          let(:opts) { { width: 300, height: 300 } }

          it 'raises ArgumentError' do
            expect { subject }.to raise_error ArgumentError
          end
        end

        context 'explicit margin' do
          let(:opts) { { margin: 30 } }

          it 'does not raise' do
            expect { subject }.to_not raise_error
          end

          describe 'the generated file' do
            before { subject }

            it 'is valid postscript' do
              expect(path).to be_valid_postscript
            end
          end
        end
      end

      let(:tempfile) { Tempfile.new('rtree-postscript') }
      let(:path) { tempfile.path }
      let(:pathname) { Pathname(path) }

      describe 'argument is an IO' do
        subject do
          File.open(tempfile, 'w') do |io|
            rtree.postscript(io, style, **opts)
          end
        end
        it_behaves_like 'it writes PostScript'
      end

      describe 'argument is a String' do
        subject { rtree.postscript(path, style, **opts) }
        it_behaves_like 'it writes PostScript'
      end

      describe 'argument is a Pathname' do
        subject { rtree.postscript(pathname, style, **opts) }
        it_behaves_like 'it writes PostScript'
      end
    end
  end
end
