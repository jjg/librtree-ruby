describe 'RTree' do
  describe '#unit_sphere_volume' do
    let(:rtree) { RTree.new(dim) }

    subject { rtree.unit_sphere_volume }

    context 'dim 2' do
      let(:dim) { 2 }

      it 'responds' do
        expect(rtree).to respond_to :unit_sphere_volume
      end

      it 'does not raise' do
        expect { subject.to_not raise_error }
      end

      it 'is a Float' do
        expect(subject).to be_a Float
      end

      it 'is π' do
        expect(subject).to be_within(1e-15).of(Math::PI)
      end
    end

    context 'dim 3' do
      let(:dim) { 3 }

      it 'responds' do
        expect(rtree).to respond_to :unit_sphere_volume
      end

      it 'does not raise' do
        expect { subject.to_not raise_error }
      end

      it 'is a Float' do
        expect(subject).to be_a Float
      end

      it 'is 4π/3' do
        expect(subject).to be_within(1e-15).of(4 * Math::PI / 3)
      end
    end
  end
end
