describe 'RTree' do
  describe '#height' do
    let(:rtree) { RTree.new(2) }

    it 'responds' do
      expect(rtree).to respond_to :height
    end

    subject { rtree.height }

    context 'in an empty R-tree' do

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an Integer' do
        expect(subject).to be_an Integer
      end

      it 'is 0' do
        expect(subject).to eq 0
      end
    end

    context 'in an R-tree of one rectangle' do
      before { rtree.add_rect(42, [0, 0, 1, 1]) }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an Integer' do
        expect(subject).to be_an Integer
      end

      it 'is 1' do
        expect(subject).to eq 1
      end
    end
  end
end
