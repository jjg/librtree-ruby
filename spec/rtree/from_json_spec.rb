describe 'RTree' do
  describe '.from_json' do

    it 'responds' do
      expect(RTree).to respond_to :from_json
    end

    context 'bad arguments' do
      subject { RTree.from_json(json) }

      describe 'nil' do
        let(:json) { nil }

        it 'raises' do
          expect { subject }.to raise_error TypeError
        end
      end

      describe 'hash' do
        let(:json) { { key: 'value' } }

        it 'raises' do
          expect { subject }.to raise_error TypeError
        end
      end

      describe 'invalid JSON' do
        let(:json) { '{' }

        it 'raises' do
          expect { subject }.to raise_error RuntimeError
        end
      end

      describe 'valid non-rtree JSON' do
        let(:json) { '{"key": "value"}' }

        it 'raises' do
          expect { subject }.to raise_error RuntimeError
        end
      end
    end

    context 'with RTree#to_json output (round trip)' do

      # this relies on RTree #to_json output, so errors here
      # may be a result of an issue in that method

      subject { RTree.from_json(json) }

      let(:instance) { RTree.new(2) }
      let(:json) { instance.to_json }

      before { nodes.each { |node| instance.add_rect(*node) } }

      context 'empty' do
        let(:nodes) { [] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is identical' do
          expect(subject).to eq instance
        end
      end

      context 'non-empty' do
        let(:nodes) do
          [
            [1, [0, 0, 1, 1]],
            [2, [1, 1, 2, 3]]
          ]
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is identical' do
          expect(subject).to eq instance
        end
      end
    end
  end
end
