describe 'RTree' do
  describe '.json_read' do

    it 'responds' do
      expect(RTree).to respond_to(:json_read)
    end

    describe 'bad argument types' do
      subject { RTree.json_read(file) }

      context 'nil' do
        let(:file) { nil }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'number' do
        let(:file) { 234 }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end
    end

    shared_examples 'it reads JSON' do

      context 'empty tree' do
        let(:nodes) { [] }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end

        it 'is empty' do
          expect(subject).to be_empty
        end
      end

      context 'non-empty tree' do
        let(:nodes) do
          [
            [1, [0, 0, 1, 1]],
            [2, [1, 1, 2, 3]]
          ]
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end

        it 'is not empty' do
          expect(subject).to_not be_empty
        end
      end
    end

    context 'with a dynamic fixture' do

      # generally one would use fixtures here, but JSON setialisation
      # is non-portable (across platforms with different page-sizes,
      # for example), so instead we create the JSON stream by reading
      # the output of #json_write, so these are round-trip tests really.

      let(:tempfile) { Tempfile.new('json-read-spec') }
      let(:path) { tempfile.path }
      let(:pathname) { Pathname.new(path) }
      let(:instance) { RTree.new(2) }

      before do
        nodes.each { |node| instance.add_rect(*node) }
        File.open(tempfile, 'w') { |file| instance.json_write(file) }
      end

      describe 'argument is valid JSON stream' do
        subject do
          File.open(path, 'r') { |file| RTree.json_read(file) }
        end
        it_should_behave_like 'it reads JSON'
      end

      describe 'argument is valid JSON path' do
        subject { RTree.json_read(path) }
        it_should_behave_like 'it reads JSON'
      end

      describe 'argument is valid JSON pathname' do
        subject { RTree.json_read(pathname) }
        it_should_behave_like 'it reads JSON'
      end
    end
  end
end
