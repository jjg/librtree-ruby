describe 'RTree' do
  describe '.csv_read' do

    it 'responds' do
      expect(RTree).to respond_to(:csv_read)
    end

    describe 'bad argument types' do
      subject { RTree.csv_read(file, 2) }

      context 'nil' do
        let(:file) { nil }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end

      context 'number' do
        let(:file) { 23 }

        it 'raises TypeError' do
          expect { subject }.to raise_error TypeError
        end
      end
    end

    shared_examples 'it reads CSV' do

      context 'empty' do
        let(:csv) { '' }

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end

        it 'is empty' do
          expect(subject).to be_empty
        end
      end

      context 'non-empty tree' do
        let(:csv) do
          <<~EOF
            1, 0, 0, 1, 1
            2, 1, 1, 2, 3
          EOF
        end

        it 'does not raise' do
          expect { subject }.to_not raise_error
        end

        it 'is an RTree' do
          expect(subject).to be_an_instance_of RTree
        end

        it 'is non-empty' do
          expect(subject).to_not be_empty
        end
      end
    end

    describe 'argument is valid CSV stream' do
      let(:tempfile) { Tempfile.new('csv-read-spec') }
      before { File.write(tempfile, csv) }
      subject { File.open(tempfile, 'r') { |io| RTree.csv_read(io, 2) } }

      it_should_behave_like 'it reads CSV'
    end

    describe 'argument is valid CSV filename' do
      let(:tempfile) { Tempfile.new('csv-read-spec') }
      before { File.write(tempfile, csv) }
      subject { RTree.csv_read(tempfile.path, 2) }

      it_should_behave_like 'it reads CSV'
    end

    describe 'argument is valid CSV Pathname' do
      let(:tempfile) { Tempfile.new('csv-read-spec') }
      before { File.write(tempfile, csv) }
      subject { RTree.csv_read(Pathname.new(tempfile.path), 2) }

      it_should_behave_like 'it reads CSV'
    end
  end
end
