require 'json'

describe 'RTree' do
  describe '#to_json' do
    let(:rtree) { RTree.new(2) }
    subject { rtree.to_json }

    it 'responds' do
      expect(rtree).to respond_to :to_json
    end

    shared_examples 'RTree #to_json' do |nodes|
      before { nodes.each { |node| rtree.add_rect(*node) } }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is a string' do
        expect(subject).to be_a String
      end

      it 'is UTF-8 encoded' do
        expect(subject.encoding).to be Encoding::UTF_8
      end

      it 'is not empty' do
        expect(subject).to_not be_empty
      end

      it 'is parseable as JSON' do
        expect { JSON.parse(subject) }.to_not raise_error
      end
    end

    context 'empty tree' do
      it_behaves_like('RTree #to_json', [])
    end

    context 'non-empty tree' do
      it_behaves_like(
        'RTree #to_json',
        [
          [1, [0, 0, 1, 1]],
          [2, [1, 1, 2, 3]]
        ]
      )
    end

    describe 'serialise pipe returns nil' do
      let(:reader) { instance_double(IO) }
      let(:writer) { instance_double(IO) }
      before do
        allow(IO).to receive(:pipe).and_return([reader, writer])
        allow(writer).to receive(:close)
        allow(reader).to receive(:fcntl)
        allow(reader).to receive(:read).and_return(nil)
        allow(reader).to receive(:close)
      end

      it 'raises IOError' do
        expect { subject }.to raise_error(IOError, /reading from pipe/)
      end
    end
  end
end
