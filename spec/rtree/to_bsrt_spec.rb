describe 'RTree' do
  describe '#to_bsrt' do
    let(:rtree) { RTree.new(2) }
    subject { rtree.to_bsrt }

    it 'responds' do
      expect(rtree).to respond_to :to_bsrt
    end

    shared_examples 'RTree #to_bsrt' do |nodes|
      before { nodes.each { |node| rtree.add_rect(*node) } }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is a string' do
        expect(subject).to be_a String
      end

      it 'is binary encoded' do
        expect(subject.encoding).to be Encoding::BINARY
      end

      it 'is not empty' do
        expect(subject).to_not be_empty
      end

      it 'has the BSRt magic' do
        expect(subject.byteslice(0, 4)).to eq 'BSRt'
      end
    end

    context 'empty tree' do
      it_behaves_like('RTree #to_bsrt', [])
    end

    context 'non-empty tree' do
      it_behaves_like(
        'RTree #to_bsrt',
        [
          [1, [0, 0, 1, 1]],
          [2, [1, 1, 2, 3]]
        ]
      )
    end
  end
end
