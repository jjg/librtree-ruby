describe 'RTree' do
  describe '#to_h' do
    let(:rtree) { RTree.new(2) }
    subject { rtree.to_h }

    it 'responds' do
      expect(rtree).to respond_to :to_h
    end

    shared_examples 'RTree #to_h' do |nodes|
      before { nodes.each { |node| rtree.add_rect(*node) } }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is a hash' do
        expect(subject).to be_a Hash
      end

      it 'is not empty' do
        expect(subject).to_not be_empty
      end

      it 'looks like an RTree hash' do
        expected_hash = {
          state: Hash,
          root: {
            branches: Array,
            count: Integer,
            level: Integer
          }
        }
        expect(subject).to match expected_hash
      end
    end

    context 'empty tree' do
      it_behaves_like('RTree #to_h', [])
    end

    context 'non-empty tree' do
      it_behaves_like(
        'RTree #to_h',
        [
          [1, [0, 0, 1, 1]],
          [2, [1, 1, 2, 3]]
        ]
      )
    end
  end
end
