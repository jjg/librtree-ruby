describe 'RTree::Style' do

  describe '.from_json' do

    subject { RTree::Style.from_json(json) }

    context 'valid style JSON' do
      let(:json) do
        <<~EOF
        [
          {
            "stroke": {
              "grey": 0.9,
              "width": 1.0
            }
          },
          {
             "fill": {
               "rgb": [0.9, 0.1, 0.1]
             }
          }
        ]
        EOF
      end

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an RTree::Style' do
        expect(subject).to be_an_instance_of RTree::Style
      end
    end

    context 'invalid JSON' do
      let(:json) { '[}' }

      it 'raise RuntimeError' do
        expect { subject }.to raise_error RuntimeError
      end
    end

    context 'style with no levels' do
      let(:json) { '[]' }

      it 'raise RuntimeError' do
        expect { subject }.to raise_error RuntimeError
      end
    end

    context 'style with empty levels' do
      let(:json) { '[{}, {}]' }

      it 'does not raise' do
        expect { subject }.to_not raise_error
      end

      it 'is an RTree::Style' do
        expect(subject).to be_an_instance_of RTree::Style
      end
    end
  end
end
