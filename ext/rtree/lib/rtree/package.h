/*
  rtree/package.h
  Copyright (c) J.J. Green 2021
*/

#ifndef RTREE_PACKAGE_H
#define RTREE_PACKAGE_H

extern const char rtree_package_version[];
extern const char rtree_package_name[];
extern const char rtree_package_url[];
extern const char rtree_package_bugreport[];

#endif
