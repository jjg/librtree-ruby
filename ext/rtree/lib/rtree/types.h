/*
  rtree/types.h
  Copyright (c) J.J. Green 2020
*/

#ifndef RTREE_TYPES_H
#define RTREE_TYPES_H

#include <stdint.h>

typedef double rtree_coord_t;
typedef uint64_t rtree_id_t;

#endif
