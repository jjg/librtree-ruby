/*
  private/constants.h
  Copyright (c) J.J. Green 2019
*/

#ifndef PRIVATE_CONSTANTS_H
#define PRIVATE_CONSTANTS_H

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifndef M_LOG_PI
#define M_LOG_PI 1.1447298858494002
#endif

#endif
