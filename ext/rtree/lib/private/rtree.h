/*
  private/rtree.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PRIVATE_RTREE_H
#define PRIVATE_RTREE_H

#include <private/state.h>
#include <private/node.h>

#include <rtree.h>

struct rtree_t
{
  state_t *state;
  node_t *root;
};

#endif
