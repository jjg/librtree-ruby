/*
  private/bounds.h
  Copyright (c) J.J. Green 2022
*/

#ifndef PRIVATE_BOUNDS_H
#define PRIVATE_BOUNDS_H

#ifndef RTREE_DIMS_MAX
#define RTREE_DIMS_MAX 256
#endif

#ifndef RTREE_LEVELS_MAX
#define RTREE_LEVELS_MAX 1024
#endif

#ifndef RTREE_BRANCHES_MAX
#define RTREE_BRANCHES_MAX 4096
#endif

#endif
