#include <ruby.h>
#include <ruby/io.h>
#include <ruby/version.h>

#include <rtree.h>
#include <rtree/package.h>
#include <rtree/postscript.h>

#include <errno.h>
#include <stdint.h>

static rb_data_type_t style_type;
static rb_data_type_t rtree_type;

static void rt_dfree(void *p)
{
  rtree_destroy((rtree_t*)p);
}

static size_t rt_dsize(const void *p)
{
  return rtree_bytes((const rtree_t*)p);
}

static VALUE rt_alloc(VALUE cls)
{
  rtree_t *rtree;
  if ((rtree = rtree_alloc()) == NULL)
    rb_raise(rb_eNoMemError, "failed to alloc rtree");
  return TypedData_Wrap_Struct(cls, &rtree_type, rtree);
}

static VALUE rt_init(VALUE self, VALUE dim_obj, VALUE flags_obj)
{
  Check_Type(dim_obj, T_FIXNUM);
  size_t dim = FIX2ULONG(dim_obj);

  Check_Type(flags_obj, T_FIXNUM);
  state_flags_t flags = FIX2UINT(flags_obj);

  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  if ((rtree_init(rtree, dim, flags)) != 0)
    rb_raise(rb_eNoMemError, "failed to init rtree");

  return self;
}

static VALUE rt_release(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);
  rtree_destroy(rtree);
  return self;
}

static VALUE rt_empty(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);
  if (rtree_empty(rtree))
    return Qtrue;
  else
    return Qfalse;
}

static VALUE rt_height(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);
  return INT2NUM(rtree_height(rtree));
}

static VALUE rt_add_rect(VALUE self, VALUE id_obj, VALUE coord_obj)
{
  Check_Type(coord_obj, T_ARRAY);
  Check_Type(id_obj, T_FIXNUM);

  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  rtree_id_t
    id = FIX2ULONG(id_obj);
  size_t
    len = RARRAY_LEN(coord_obj),
    dim = rtree_dims(rtree);

  if (len != 2 * dim)
    rb_raise(rb_eArgError, "expected array length %zi, got %zi", 2 * dim, len);

  rtree_coord_t coord[len];

  for (size_t i = 0 ; i < len ; i++)
    coord[i] = NUM2DBL(rb_ary_entry(coord_obj, i));

  int err;

  if ((err = rtree_add_rect(rtree, id, coord)) != 0)
    {
      if (errno)
        rb_sys_fail("add_rect");
      else
        rb_raise(rb_eRuntimeError, "%s", rtree_strerror(err));
    }

  return self;
}

/* update rectangles in the tree without changing its structure */

static int update_cb(rtree_id_t id, rtree_coord_t *coord, void *context)
{
  size_t len = *(size_t*)context;
  VALUE id_obj = INT2NUM(id);
  VALUE coord_obj = rb_ary_new2(len);

  for (size_t i = 0 ; i < len ; i++)
    rb_ary_store(coord_obj, i, DBL2NUM(coord[i]));

  coord_obj = rb_yield_values(2, id_obj, coord_obj);

  Check_Type(coord_obj, T_ARRAY);
  if (len != (size_t)RARRAY_LEN(coord_obj))
    rb_raise(rb_eArgError, "Array of wrong size");

  for (size_t i = 0 ; i < len ; i++)
    coord[i] = NUM2DBL(rb_ary_entry(coord_obj, i));

  return 0;
}

static VALUE rt_update(VALUE self)
{
  if (!rb_block_given_p())
    rb_raise(rb_eArgError, "Expected block");

  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  size_t len = 2 * rtree_dims(rtree);
  int err = rtree_update(rtree, update_cb, &len);

  if (err != 0)
    rb_raise(rb_eRuntimeError, "librtree: %s", strerror(err));

  return self;
}

/*
  The librtree API expects the search callback to return zero to
  continue the search, non-zero to terminate.  I was expecting to
  need to wrap the 'yield' in 'rescue's and 'ensure's to handle
  exceptions and breaks, but find that doing nothing actually does
  the right thing (see specs).  The search implementation is just
  a recursive search through the tree (as you would expect) so
  there is no end-of-search cleanup to do, so I think this is all
  just fine ... wild
*/

static int search_cb(rtree_id_t id, void *context)
{
  rb_yield(INT2NUM(id));
  return 0;
}

static VALUE rt_search(VALUE self, VALUE coord_obj)
{
  if (!rb_block_given_p())
    rb_raise(rb_eArgError, "Expected block");

  Check_Type(coord_obj, T_ARRAY);

  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  size_t
    len = RARRAY_LEN(coord_obj),
    dim = rtree_dims(rtree);

  if (len != 2 * dim)
    rb_raise(rb_eArgError, "expected array length %zi, got %zi", 2 * dim, len);

  rtree_coord_t coord[len];

  for (size_t i = 0 ; i < len ; i++)
    coord[i] = NUM2DBL(rb_ary_entry(coord_obj, i));

  rtree_search(rtree, coord, search_cb, NULL);

  return Qnil;
}

/* deserialisation */

typedef rtree_t* (deserialise_t)(FILE*);

static VALUE deserialise(VALUE cls, VALUE io_obj, deserialise_t *f)
{
  Check_Type(io_obj, T_FILE);

  rb_io_t *io;
  GetOpenFile(io_obj, io);
  rb_io_check_initialized(io);
  rb_io_check_readable(io);
  FILE *fp = rb_io_stdio_file(io);

  rtree_t *rtree;
  errno = 0;
  if ((rtree = f(fp)) == NULL)
    {
      if (errno)
        rb_sys_fail(__func__);
      else
        rb_raise(rb_eRuntimeError, "Failed read from stream");
    }

  return TypedData_Wrap_Struct(cls, &rtree_type, rtree);
}

static VALUE rt_json_read(VALUE cls, VALUE io_obj)
{
  return deserialise(cls, io_obj, rtree_json_read);
}

static VALUE rt_bsrt_read(VALUE cls, VALUE io_obj)
{
  return deserialise(cls, io_obj, rtree_bsrt_read);
}

static VALUE rt_csv_read(VALUE cls,
                         VALUE io_obj, VALUE dim_obj, VALUE flags_obj)
{
  Check_Type(io_obj, T_FILE);
  rb_io_t *io;
  GetOpenFile(io_obj, io);
  rb_io_check_initialized(io);
  rb_io_check_readable(io);
  FILE *fp = rb_io_stdio_file(io);

  Check_Type(dim_obj, T_FIXNUM);
  size_t dim = FIX2ULONG(dim_obj);

  Check_Type(flags_obj, T_FIXNUM);
  state_flags_t flags = FIX2UINT(flags_obj);

  rtree_t *rtree;
  errno = 0;
  if ((rtree = rtree_csv_read(fp, dim, flags)) == NULL)
    {
      if (errno)
        rb_sys_fail(__func__);
      else
        rb_raise(rb_eRuntimeError, "Failed read from stream");
    }

  return TypedData_Wrap_Struct(cls, &rtree_type, rtree);
}

/* serialisation */

typedef int (serialise_t)(const rtree_t*, FILE*);

static VALUE serialise(VALUE self, VALUE io_obj, serialise_t *f)
{
  Check_Type(io_obj, T_FILE);

  rb_io_t *io;
  GetOpenFile(io_obj, io);
  rb_io_check_initialized(io);
  rb_io_check_writable(io);
  FILE *fp = rb_io_stdio_file(io);

  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  int err = f(rtree, fp);
  if (err != 0)
    rb_raise(rb_eRuntimeError, "librtree: %s", rtree_strerror(err));

  return self;
}

static VALUE rt_bsrt_write(VALUE self, VALUE io_obj)
{
  return serialise(self, io_obj, rtree_bsrt_write);
}

static VALUE rt_csv_write(VALUE self, VALUE io_obj)
{
  return serialise(self, io_obj, rtree_csv_write);
}

static VALUE rt_json_write(VALUE self, VALUE io_obj)
{
  return serialise(self, io_obj, rtree_json_write);
}

static VALUE rt_identical(VALUE self, VALUE other)
{
  rtree_t *rtree_self, *rtree_other;

  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree_self);
  TypedData_Get_Struct(other, rtree_t, &rtree_type, rtree_other);

  if (rtree_identical(rtree_self, rtree_other))
    return Qtrue;
  else
    return Qfalse;
}

static VALUE rt_clone(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  rtree_t *clone;
  errno = 0;
  if ((clone = rtree_clone(rtree)) == NULL)
    {
      if (errno)
        rb_sys_fail(__func__);
      else
        rb_raise(rb_eRuntimeError, "Failed clone");
    }

  return TypedData_Wrap_Struct(CLASS_OF(self), &rtree_type, clone);
}

static VALUE state_size_access(VALUE self, size_t (*f)(const rtree_t*))
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);
  return INT2NUM(f(rtree));
}

static VALUE rt_dim(VALUE self)
{
  return state_size_access(self, rtree_dims);
}

static VALUE rt_page_size(VALUE self)
{
  return state_size_access(self, rtree_page_size);
}

static VALUE rt_node_size(VALUE self)
{
  return state_size_access(self, rtree_node_size);
}

static VALUE rt_rect_size(VALUE self)
{
  return state_size_access(self, rtree_rect_size);
}

static VALUE rt_branch_size(VALUE self)
{
  return state_size_access(self, rtree_branch_size);
}

static VALUE rt_branching_factor(VALUE self)
{
  return state_size_access(self, rtree_branching_factor);
}

static VALUE rt_unit_sphere_volume(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);
  return DBL2NUM(rtree_unit_sphere_volume(rtree));
}

static VALUE rt_size(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);
  return INT2NUM(rtree_bytes(rtree));
}

static VALUE rt_envelope(VALUE self)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  if (rtree_empty(rtree))
    return Qnil;

  size_t n = 2 * rtree_dims(rtree);
  double u[n];

  if (rtree_envelope(rtree, u) != 0)
    rb_sys_fail(__func__);

  VALUE v = rb_ary_new_capa(n);

  for (size_t i = 0 ; i < n ; i++)
    rb_ary_store(v, i, DBL2NUM(u[i]));

  return v;
}

static VALUE rt_version(VALUE self)
{
  return rb_str_new_cstr(rtree_package_version);
}

static VALUE rt_bugreport(VALUE self)
{
  return rb_str_new_cstr(rtree_package_bugreport);
}

static VALUE rt_url(VALUE self)
{
  return rb_str_new_cstr(rtree_package_url);
}

static VALUE rt_postscript(VALUE self,
                           VALUE style_obj,
                           VALUE axis_obj,
                           VALUE extent_obj,
                           VALUE margin_obj,
                           VALUE io_obj)
{
  rtree_t *rtree;
  TypedData_Get_Struct(self, rtree_t, &rtree_type, rtree);

  postscript_style_t *style;
  TypedData_Get_Struct(style_obj, postscript_style_t, &style_type, style);

  Check_Type(io_obj, T_FILE);
  rb_io_t *io;
  GetOpenFile(io_obj, io);
  rb_io_check_initialized(io);
  rb_io_check_writable(io);
  FILE *fp = rb_io_stdio_file(io);

  float
    extent = NUM2DBL(extent_obj),
    margin = NUM2DBL(margin_obj);

  Check_Type(axis_obj, T_FIXNUM);
  extent_axis_t axis = FIX2UINT(axis_obj);

  rtree_postscript_t opt = {
    .style = style,
    .axis = axis,
    .extent = extent,
    .margin = margin,
    .title = "librtree-ruby output"
  };

  int err;
  if ((err = rtree_postscript(rtree, &opt, fp)) != 0)
    rb_raise(rb_eRuntimeError, "librtree: %s", rtree_strerror(err));

  return Qnil;
}

static rb_data_type_t rtree_type = {
  .wrap_struct_name = "rtree-wrap",
  .function = {
    .dmark = NULL,
    .dfree = rt_dfree,
    .dsize = rt_dsize,
#if RUBY_API_VERSION_CODE < 20700
    .reserved = { NULL, NULL }
#else
    .dcompact = NULL,
    .reserved = { NULL }
#endif
  },
  .parent = NULL,
  .data = NULL,
  .flags = RUBY_TYPED_FREE_IMMEDIATELY
};

/* RTree::Style */

static void st_dfree(void *p)
{
  postscript_style_destroy((postscript_style_t*)p);
}

static VALUE st_release(VALUE self)
{
  postscript_style_t *style;
  TypedData_Get_Struct(self, postscript_style_t, &style_type, style);
  postscript_style_destroy(style);
  return self;
}

static VALUE st_json_read(VALUE cls, VALUE io_obj)
{
  Check_Type(io_obj, T_FILE);

  rb_io_t *io;
  GetOpenFile(io_obj, io);
  rb_io_check_initialized(io);
  rb_io_check_readable(io);
  FILE *fp = rb_io_stdio_file(io);

  postscript_style_t *style;
  errno = 0;

  if ((style = postscript_style_read(fp)) == NULL)
    {
      if (errno)
        rb_sys_fail(__func__);
      else
        rb_raise(rb_eRuntimeError, "Failed read from stream");
    }

  return TypedData_Wrap_Struct(cls, &style_type, style);
}

static rb_data_type_t style_type = {
  .wrap_struct_name = "style-wrap",
  .function = {
    .dmark = NULL,
    .dfree = st_dfree,
    .dsize = NULL,
#if RUBY_API_VERSION_CODE < 20700
    .reserved = { NULL, NULL }
#else
    .dcompact = NULL,
    .reserved = { NULL }
#endif
  },
  .parent = NULL,
  .data = NULL,
  .flags = RUBY_TYPED_FREE_IMMEDIATELY
};

void Init_rtree(void)
{
  VALUE cRTreeBase = rb_const_get(rb_cObject, rb_intern("RTreeBase"));

  rb_define_alloc_func(cRTreeBase, rt_alloc);
  rb_define_method(cRTreeBase, "initialize", rt_init, 2);
  rb_define_method(cRTreeBase, "free", rt_release, 0);
  rb_define_method(cRTreeBase, "clone", rt_clone, 0);
  rb_define_method(cRTreeBase, "update!", rt_update, 0);
  rb_define_method(cRTreeBase, "empty?", rt_empty, 0);
  rb_define_method(cRTreeBase, "height", rt_height, 0);
  rb_define_method(cRTreeBase, "add_rect", rt_add_rect, 2);
  rb_define_method(cRTreeBase, "search", rt_search, 1);
  rb_define_method(cRTreeBase, "bsrt_write", rt_bsrt_write, 1);
  rb_define_method(cRTreeBase, "csv_write", rt_csv_write, 1);
  rb_define_method(cRTreeBase, "json_write", rt_json_write, 1);
  rb_define_method(cRTreeBase, "eq?", rt_identical, 1);
  rb_define_method(cRTreeBase, "dim", rt_dim, 0);
  rb_define_method(cRTreeBase, "size", rt_size, 0);
  rb_define_method(cRTreeBase, "envelope", rt_envelope, 0);
  rb_define_method(cRTreeBase, "page_size", rt_page_size, 0);
  rb_define_method(cRTreeBase, "node_size", rt_node_size, 0);
  rb_define_method(cRTreeBase, "rect_size", rt_rect_size, 0);
  rb_define_method(cRTreeBase, "branch_size", rt_branch_size, 0);
  rb_define_method(cRTreeBase, "branching_factor", rt_branching_factor, 0);
  rb_define_method(cRTreeBase, "unit_sphere_volume", rt_unit_sphere_volume, 0);
  rb_define_method(cRTreeBase, "postscript", rt_postscript, 5);
  rb_define_singleton_method(cRTreeBase, "version", rt_version, 0);
  rb_define_singleton_method(cRTreeBase, "bugreport", rt_bugreport, 0);
  rb_define_singleton_method(cRTreeBase, "url", rt_url, 0);
  rb_define_singleton_method(cRTreeBase, "json_read", rt_json_read, 1);
  rb_define_singleton_method(cRTreeBase, "bsrt_read", rt_bsrt_read, 1);
  rb_define_singleton_method(cRTreeBase, "csv_read", rt_csv_read, 3);
  rb_define_const(cRTreeBase, "SPLIT_QUADRATIC", INT2NUM(RTREE_SPLIT_QUADRATIC));
  rb_define_const(cRTreeBase, "SPLIT_LINEAR", INT2NUM(RTREE_SPLIT_LINEAR));
  rb_define_const(cRTreeBase, "SPLIT_GREENE", INT2NUM(RTREE_SPLIT_GREENE));
  rb_define_const(cRTreeBase, "AXIS_HEIGHT", INT2NUM(axis_height));
  rb_define_const(cRTreeBase, "AXIS_WIDTH", INT2NUM(axis_width));

  VALUE cRTreeStyleBase = rb_const_get(rb_cObject, rb_intern("RTreeStyleBase"));

  rb_undef_alloc_func(cRTreeStyleBase);
  rb_define_method(cRTreeStyleBase, "free", st_release, 0);
  rb_define_singleton_method(cRTreeStyleBase, "json_read", st_json_read, 1);
}
