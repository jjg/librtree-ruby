librtree-ruby
-------------

A Ruby native extension implementing the R-tree spatial index of
Guttman-Green.  The code is an embedded version of [librtree][3].


### Installing dependencies

Just [jansson][1]: this common library may be in your operating-system's
repositories. On Debian, Ubuntu and derivatives:

    apt-get install libjansson-dev

On RedHat, CentOS:

    yum install jansson-devel

On OSX:

    brew install jansson

One does not need to install `librtree` itself, the library-code is
embedded within the gem (as of version 1.0.0).


### Install the Gem

Add

    gem 'librtree'

to your project's `Gemfile` and run `bundle install`.


### Use the RTree class

In your Ruby code

    require 'rtree'

will import the **RTree** class, its usage is described [here][5].


### Development setup

This is only needed if you want work on the extension, not for
a Gem install.

First install the dependencies as above.  Then clone the repository and
install the extension's Ruby dependencies, there are several ways to do
this.

If using [rbenv][4], then first set a local version of Ruby to use,
then install the gems into your local cache

    rbenv local 3.2.0
    bundle install

The repository's `.gitignore` ignores **rbenv**'s `.ruby-version` file,
since we don't want to mandate a particular Ruby version for the package
(although 3.0 is the minimal version for development at present).

If not using **rbenv** then just

    bundle install

should attempt to install the gems into your system location, which
will typically require `sudo`.  If you don't want to (or cannot) do
this, then

    bundle install --path vendor/bundle

will install into `vendor` and set the location in `.bundle/config`,
both of these are ignored by the repository's Git configuration.

Finally, you should be in a position to compile the package and
run the tests:

    bundle exec rake compile
    bundle exec rake spec
    bundle exec steep check


[1]: http://www.digip.org/jansson/
[3]: https://jjg.gitlab.io/en/code/librtree/
[4]: https://github.com/rbenv/rbenv
[5]: https://www.rubydoc.info/gems/librtree/RTree
